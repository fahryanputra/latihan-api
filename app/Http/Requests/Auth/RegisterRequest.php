<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'nama' => ['required', 'unique: users, nama'],
          'email' => ['email', 'required', 'unique: users, email'],
          'password' => ['required', 'min: 6'],
          'nim' => ['required', 'unique: users, nim'],
          'fakultas' => ['required', 'unique: users, fakultas'],
          'jurusan' => ['required', 'unique: users, jurusan'],
          'nomor_hp' => ['required', 'unique: users, nomor_hp'],
          'nomor_wa' => ['required', 'unique: users, nomor_wa']
        ];
    }
}
