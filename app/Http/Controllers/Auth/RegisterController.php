<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        User::create([
          'nama' => request('nama'),
          'email' => request('email'),
          'password' => bcrypt(request('password')),
          'nim' => request('nim'),
          'fakultas' => request('fakultas'),
          'jurusan' => request('jurusan'),
          'nomor_hp' => request('nomor_hp'),
          'nomor_wa' => request('nomor_wa')
        ]);

        return response('Terima kasih, anda sudah teradaftar.');
    }
}
